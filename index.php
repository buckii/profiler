<!DOCTYPE html>
<html lang="en">
<head>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
body {
  padding-top: 60px;
}
@media (max-width: 979px) {
  body {
    padding-top: 0px;
  }
}
h2 {
	padding: 0 10px;
	background: #eee;
	border: 1px solid #ccc;
	border-left: none;
	border-right: none;
}
.section {
	padding-top: 45px;
}
.section .inner {
	margin: 0 2%;
}
#search {
	margin: 0 10px;
}
</style>
</head>
<body data-spy="scroll" data-target="#navbar" data-offset="200">

<div id="main-container">
          <div id="navbar" class="navbar navbar-static navbar-fixed-top">
            <div class="navbar-inner">
              <div class="container" style="width: auto;">
                <a class="brand" href="#">buckii</a>
                <ul class="nav">
                  <li class=""><a href="#links">links</a></li>
                  <li class=""><a href="#whois">whois</a></li>
                  <li class=""><a href="#dig">dig</a></li>
                  <li class=""><a href="#ipwhois">ip whois</a></li>
                  <li class=""><a href="#mx">mx</a></li>
                  <li class=""><a href="#txt">spf</a></li>
                  <li class=""><a href="#http">http</a></li>
                  <li class=""><a href="#w3techs">w3techs</a></li>
<!--
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Databases<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li class=""><a href="#mysql">MySQL</a></li>
                      <li class=""><a href="#pgsql">PostgreSQL</a></li>
                     <li class=""><a href="#mgdb">MongoDB</a></li>
		     </ul>
                  </li>
-->
                </ul>
              </div>
            </div>
          </div>
<div id="inner">
<?php
$q_sanitized = preg_grep('/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix',array($_GET['q']));
if(!empty($q_sanitized)) {
	$q = $q_sanitized[0];
} else if(isset($_GET['q'])) {
	echo '<p style="color:#c00;">Invalid input</p>';
}

echo '<form id="search" method="get"><input name="q" value="'.(empty($q) ? '' : $q).'"><input type="submit" value="Go" /></form>';
if(!empty($q)) {
	$whois = shell_exec("whois $q");
	$dig = shell_exec("dig $q");
	$mx = shell_exec("dig $q mx");
	$txt = shell_exec("dig $q txt");
	$ip = shell_exec("dig +short $q");
	$ipwhois = shell_exec("whois $ip");

/*
 * Homepage Headers & Body
 */
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'http://'.$q);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

	$response = curl_exec($ch);
	$response_escaped = htmlspecialchars($response);

	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$header = substr($response, 0, $header_size);
	$body = substr($response, $header_size);
	$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
	$last_url = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);

	//get the 99th page of Google search results because they only provide up to 1000 results (10 per page)
	$google_base = "https://www.google.com/search?q=";
	$google_all = "site:$q&start=990";
	$google_nodocs = "site:$q+-filetype:pdf+-filetype:doc+-filetype:xls+-filetype:docx+-filetype:xlsx&start=990";

	echo "<div id=\"links\" class=\"section\"><h2>links</h2><div class=\"inner\">"
		."<p><a target=\"_blank\" href=\"$google_base$google_all\">Google: all file types</a></p>"
		."<p><a target=\"_blank\" href=\"$google_base$google_nodocs\">Google: exclude documents (pdf, doc, docx)</a></p>"
		."<p><a target=\"_blank\" href=\"http://wwwalexa.com/siteinfo/$q\">Alexa for $q</a></p>"
		."<p><a target=\"_blank\" href=\"http://marketing.grader.com/site/$q\">Website Grader for $q</a></p>"
		."</div></div>";
	echo "<div id=\"whois\" class=\"section\"><h2>whois</h2><div class=\"inner\"><pre>$whois</pre></div>";
	echo "<div id=\"dig\" class=\"section\"><h2>dig</h2><div class=\"inner\"><pre>$dig</pre></div>";
	echo "<div id=\"ipwhois\" class=\"section\"><h2>ip whois</h2><div class=\"inner\"><pre>$ipwhois</pre></div>";
	echo "<div id=\"mx\" class=\"section\"><h2>MX</h2><div class=\"inner\"><pre>$mx</pre></div>";
	echo "<div id=\"txt\" class=\"section\"><h2>SPF</h2><div class=\"inner\"><pre>$txt</pre></div>";
	echo "<div id=\"http\" class=\"section\"><h2>HTTP Response</h2><div class=\"inner\"><pre>$response_escaped</pre></div>";

	/* W3Techs.com */
	echo "<div id=\"w3techs\" class=\"section\"><h2>W3Techs</h2><div class=\"inner\">".
		'<div id="w3t" class="w3t">
		<a href="http://w3techs.com/sites/info/q?url='.$q.'">W3Techs</a>
		shows which technology the site is using
		</div>

		<script src="http://w3techs.com/siteinfo.js?url='.$q.'"></script>
		<script>w3t_si(document.getElementById("w3t"))</script>
		</div>';
		;
}
?>
</div> <!-- /#inner -->
</div> <!-- /#main-container -->
</body>
</html>
